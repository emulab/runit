This is the Emulab packaging of runit-2.1.2 + customizations, for rpm
and deb.  This is a fork of a couple packaging repos; but the rpm build
has been refactored to build runit using the Debian patchset, to ensure
we get a consistent build across distros.  Plus we add our own patches.
I don't want to have to track all this stuff in a separate repo per
fork, so it is subtree merged.  All choices were bad.

The rpm packaging is very simple; it comes from
github.com/imeyer/runit-rpm , and is modified to work against the Debian
patchset.  runit is not packaged by the distro maintainers for any
RPM-based distro, AFAIK -- so there's no need to track upstream for any
kind of consistency on distros.

The deb packaging is a bit more complex.  It comes from
https://anonscm.debian.org/cgit/users/kaction-guest/runit.git .  runit
has been cranking along in the Debian repos for a long time with minor
caretaking; the packaging is pretty simple and just builds on many
versions of Debian/Ubuntu/etc.  However, more recently, the new
maintainer has made many cleanups.  Most of these are helpful to us, but
some also prevent the package from building on older Debian/Ubuntus.
Unlike Redhat spec files, deb src packages don't have a good way to
conditionalize build deps.  Moreover, he has split the package into
runit and runit-init (the latter of which conflicts with any other init
-- and we don't want that because we don't want to conflict with systemd
or upstart -- we are happy if init lives in /sbin/runit-init, not in
/sbin/init).  Thus it is unlikely we'll be concerned with tracking
upstream, and if we do, we'll have to do some kind of versioned approach
(i.e., migrate current debian/ to debian-legacy and re-pull from
upstream into debianN, or similar).
